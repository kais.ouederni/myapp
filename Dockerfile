# Utiliser une image de base officielle Python
FROM python:3.10-slim

# Définir le répertoire de travail dans le conteneur
WORKDIR /app

# Copier les fichiers nécessaires dans le conteneur
COPY requirements.txt .
COPY app.py .

# Installer les dépendances
RUN pip install -r requirements.txt

# Exposer le port pour Prometheus
EXPOSE 8000

# Commande pour démarrer l'application
CMD ["python", "app.py"]
