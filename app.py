from prometheus_client import start_http_server, Counter
import time

# Création d'une métrique de type compteur
c = Counter('mes_visites', 'Nombre de visites')

def process_request():
    """Simule un traitement d'une requête en incrémentant le compteur."""
    c.inc()

if __name__ == '__main__':
    # Démarre un serveur pour exposer les métriques sur le port 8000
    start_http_server(8000)
    # Boucle infinie pour simuler un traitement de requêtes
    while True:
        process_request()
        time.sleep(1)  # Pause d'une seconde pour simuler le temps de traitement
